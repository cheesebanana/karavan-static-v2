module.exports = function(grunt) {

  grunt.initConfig({

    concat: {
      plugins: {
        src: 'js/plugins/*.js',
        dest: 'js/plugins.js'
      },

      script: {
        src:'js/script/*.js',
        dest: 'js/script.js'
      },

      base: {
        options: {
          banner: '@charset "utf-8"; @import "compass";\n\n',
        },
        src: 'scss/base/*.scss',
        dest: 'scss/base.scss'
      },

      style: {
        options: {
          banner: '@charset "utf-8"; @import "compass";\n\n',
        },
        src: 'scss/style/*.scss',
        dest: 'scss/style.scss'
      },

      blocks: {
        src: 'swig/blocks/*.swig',
        dest: 'swig/blocks.swig'
      }
    },

    uglify: {
      plugins: {
        files: {
          'js/plugins.min.js': '<%= concat.plugins.dest %>'
        }
      },

      script: {
        files: {
          'js/script.min.js': '<%= concat.script.dest %>'
        }
      }
    },

    compass: {
      base: {
        options: {
          sassDir: 'scss',
          specify: 'scss/base.scss',
          cssDir: 'css',
          environment: 'production',
          outputStyle: 'compressed',
          noLineComments: true
        }
      },

      style: {
        options: {
          sassDir: 'scss',
          specify: 'scss/style.scss',
          cssDir: 'css',
          environment: 'production',
          outputStyle: 'compact',
          noLineComments: true
        }
      }
    },

    swig: {
      view: {
        src: 'swig/blocks/*.view',
        dest: 'html',
        url: '../../',
        generateSitemap: false,
        generateRobotstxt: false
      },

      pages: {
        src: 'swig/pages/*.swig',
        dest: 'html',
        url: '../../',
        generateSitemap: false,
        generateRobotstxt: false
      }
    },

    watch: {
      plugins: {
        files: '<%= concat.plugins.src %>',
        tasks: [
          'concat:plugins',
          'uglify:plugins'
        ]
      },

      script: {
        files: '<%= concat.script.src %>',
        tasks: [
          'concat:script',
          'uglify:script'
        ]
      },

      base: {
        files: '<%= concat.base.src %>',
        tasks: [
          'concat:base',
          'compass:base'
        ]
      },

      style: {
        files: '<%= concat.style.src %>',
        tasks: [
          'concat:style',
          'compass:style'
        ]
      },

      blocks: {
        files: '<%= concat.blocks.src %>',
        tasks: [
          'concat:blocks',
          'swig:view',
          'swig:pages'
        ]
      },

      view: {
        files: '<%= swig.view.src %>',
        tasks: 'swig:view'
      },

      pages: {
        files: '<%= swig.pages.src %>',
        tasks: 'swig:pages'
      },

      layout: {
        files: 'swig/layout.swig',
        tasks: 'swig'
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-swig'); 
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('default', ['concat', 'uglify', 'compass', 'swig', 'watch']);

};
